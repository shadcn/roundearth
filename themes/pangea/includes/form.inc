<?php
/**
 * @file
 * Theme and preprocess functions for forms
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 */
function pangea_form_roundearth_search_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Add the navbar-form class.
  $form['#attributes']['class'][] = 'navbar-form';

  // Update the placeholder and size of field.
  $form['keywords']['#title'] = '';
  $form['keywords']['#size'] = 20;
  $form['keywords']['#placeholder'] = t('Search');
}
