<?php
/**
 * @file
 * Theme and preprocess functions for views
 */

/**
 * Implements THEMENAME_preprocess_views_view_field().
 */
function pangea_preprocess_views_view_field(&$variables) {
  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  $field = $variables['field']->field;

  if ($view->id() == 'roundearth_events_upcoming' && $view->current_display == 'block_1') {
    switch ($field) {
      // Get the date from the field and format it for the template.
      case 'start_date':
        $date = $variables['row']->civicrm_event_start_date;
        $timestamp = strtotime($date);
        $variables['day'] = date('d', $timestamp);
        $variables['month'] = date('F', $timestamp);
        break;

      // Fix escaped HTML charactes.
      case 'summary':
        $summary = html_entity_decode($variables['row']->civicrm_event_summary);

        // Remove h3 header from legacy content.
        $summary = preg_replace('#<h3>(.*?)</h3>#', '', $summary, 1);

        $variables['output'] = [
          '#markup' => substr(strip_tags($summary), 0, 40) . '...',
        ];
        break;
    }
  }
}
