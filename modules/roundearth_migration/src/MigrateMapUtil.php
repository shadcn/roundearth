<?php

namespace Drupal\roundearth_migration;

use Drupal\migrate\Plugin\MigrationPluginManagerInterface;

/**
 * Class MigrateMapUtil.
 */
class MigrateMapUtil {

  /**
   * Migrate plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationManager;

  /**
   * MigrateMapUtil constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migrationManager
   *   Migrate plugin manager.
   */
  public function __construct(MigrationPluginManagerInterface $migrationManager) {
    $this->migrationManager = $migrationManager;
  }

  /**
   * Gets the map for a given migration.
   *
   * @param string $migrationId
   *   Migration ID.
   *
   * @return \Drupal\migrate\Plugin\MigrateIdMapInterface
   *   Migration ID map.
   */
  public function getMigrationMap($migrationId) {
    return $this->migrationManager->createInstance($migrationId)->getIdMap();
  }

  /**
   * Gets the destination row for a given source.
   *
   * @param mixed $sourceId
   *   The source ID, may be either a scalar value or an array.
   * @param array $migrationIds
   *   An array of migration IDs to analyze.
   *
   * @return array
   *   The destination row.
   */
  public function lookupDestIdRow($sourceId, array $migrationIds) {
    if (!is_array($sourceId)) {
      $sourceId = [$sourceId];
    }

    foreach ($migrationIds as $migrationId) {
      $destIds = $this->getMigrationMap($migrationId)
        ->lookupDestinationIds($sourceId);
      if ($destIds) {
        return reset($destIds);
      }
    }

    return [];
  }

  /**
   * Gets the destination ID.
   *
   * @param mixed $sourceId
   *   The source ID, may be either a scalar value or an array.
   * @param array $migrationIds
   *   An array of migration IDs to analyze.
   *
   * @return mixed
   *   A scalar destination ID, or NULL if not found.
   */
  public function lookupDestId($sourceId, array $migrationIds) {
    return reset($this->lookupDestIdRow($sourceId, $migrationIds));
  }

  /**
   * Gets the destination row for a given source.
   *
   * @param mixed $destId
   *   The destination ID, may be either a scalar value or an array.
   * @param array $migrationIds
   *   An array of migration IDs to analyze.
   *
   * @return array
   *   The source row.
   */
  public function lookupSourceIdRow($destId, array $migrationIds) {
    if (!is_array($destId)) {
      $destId = [$destId];
    }

    foreach ($migrationIds as $migrationId) {
      $sourceId = $this->getMigrationMap($migrationId)
        ->lookupSourceID($destId);
      if ($sourceId) {
        return $sourceId;
      }
    }

    return [];
  }

  /**
   * Gets the destination ID.
   *
   * @param mixed $destId
   *   The destination ID, may be either a scalar value or an array.
   * @param array $migrationIds
   *   An array of migration IDs to analyze.
   *
   * @return mixed
   *   A scalar source ID, or NULL if not found.
   */
  public function lookupSourceId($destId, array $migrationIds) {
    return reset($this->lookupSourceIdRow($destId, $migrationIds));
  }

}
