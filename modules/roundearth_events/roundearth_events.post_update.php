<?php

/**
 * @file
 * Post update functions for Roundearth Events.
 */

/**
 * Move field_roundearth_events_speaker to multi value speaker reference field.
 */
function roundearth_events_post_update_copy_legacy_speaker_data(&$sandbox = NULL) {
  // Find all paragraphs where a speaker is set.
  $storage = Drupal::entityTypeManager()->getStorage('paragraph');
  $ids = $storage->getQuery()
    ->condition('field_roundearth_events_speaker', NULL, 'IS NOT NULL')
    ->execute();

  if (count($ids)) {
    $paragraphs = $storage->loadMultiple($ids);

    // For each paragraph get the value of the legacy speaker field, create a
    // new speaker paragraph and move this field reference value to it.
    $count = 0;
    foreach ($paragraphs as $session) {
      /** @var CivicrmEntity $speaker */
      if (($session->hasField('field_roundearth_events_speaker')) && ($speaker = $session->get('field_roundearth_events_speaker')->entity)) {
        // Create a new speaker paragraph.
        /** @var \Drupal\paragraphs\ParagraphInterface $event_speaker */
        $event_speaker = $storage->create([
          'type' => 'roundearth_events_speaker',
        ]);
        $event_speaker->set('field_roundearth_events_spk', [
          'target_id' => $speaker->id(),
        ]);
        $event_speaker->setParentEntity($session, 'field_roundearth_events_speakers');
        $event_speaker->save();

        $session->field_roundearth_events_speakers->entity = $event_speaker;
        $session->save();
        $count++;
      }
    }

    return t('Successfully updated speaker values for @count sessions.', [
      '@count' => $count,
    ]);
  }

  return t('No sessions updated.');
}
